// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore"
import { getAuth } from "firebase/auth";
import { Timestamp  } from "firebase/firestore";

// const firebaseConfig = {
//   apiKey: "AIzaSyCIUQ6ii4QjalUQwj_KoIKt3YykZdeIV8s",
//   authDomain: "note-easy-e1be0.firebaseapp.com",
//   projectId: "note-easy-e1be0",
//   storageBucket: "note-easy-e1be0.appspot.com",
//   messagingSenderId: "86339389228",
//   appId: "1:86339389228:web:33865ae677abce5ccd4e89",
//   measurementId: "G-FTFD90X25X"
// };

const firebaseConfig = {
  apiKey: "AIzaSyBLm9vaSSahwibjhLHsAPoOjsV-Jnrr-XI",
  authDomain: "note-easy-app.firebaseapp.com",
  projectId: "note-easy-app",
  storageBucket: "note-easy-app.appspot.com",
  messagingSenderId: "270648946056",
  appId: "1:270648946056:web:b90647307abfb2c4f0de3d",
  measurementId: "G-V5VH3391CZ"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app)
const auth = getAuth();

export { db, auth, Timestamp };